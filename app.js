var keypress = require('keypress');
var noble = require('noble');

keypress(process.stdin);

// This will contain all the drones
var dronelist = [];
var readylist = [];

droneStats = {
  flying: false
};

var connectionTimeoutReached = false;
var connectionsRequested = false;

// Find all drones, and take them over.
noble.on('stateChange', function(state) {
  if (state === 'poweredOn') {
    console.log('The robo bouncers are letting people in!');
    noble.startScanning();
    setTimeout(function(){
      // find as may drones as we can, then start the party
      console.log('We have ' + dronelist.length + ' drone(s) in our party');

      noble.stopScanning();
      droneParty();
    }, 5000);
  } else {
    noble.stopScanning();
  }
});

noble.on('discover', function (peripheral) {
  if(peripheral.advertisement.localName){
    if(peripheral.advertisement.localName.substring(0, 2) === 'RS'){
      console.log('Pushing ' + peripheral.advertisement.localName);
      dronelist.push( new RollingSpider({uuid: peripheral.uuid}) );
    }
  }
});

var RollingSpider = require("rolling-spider");

// dispatch commands to all connected drones
var Drones = function(options){
  console.log('Running command: ' + options.fun + ' on all drones!');
  dronelist.map(function(drone, index){
    if(options.arg){
      drone[options.fun](options.arg);
    }else{
      drone[options.fun]();
    }
  });
}

function droneParty(){
  if(readylist.length === dronelist.length || connectionTimeoutReached){
    console.log('Looks like all the drones are ready.')
    Drones({fun: 'flatTrim'});
    Drones({fun: 'takeOff'});

    process.stdin.on('keypress', function (ch, key) {
      switch(key.sequence){
        case 'w':
          Drones({fun: 'forward', arg: 50});
          break;

        case 's':
          Drones({fun: 'backward', arg: 50});
          break;

        case 'a':
          Drones({fun: 'left', arg: 50});
          break;

        case 'd':
          Drones({fun: 'right', arg: 50});
          break;

        case 'q':
          Drones({fun: 'hover'});
          break;

        case 'e':
          if(droneStats.flying){
            console.log('taking off');
            droneStats.flying = !droneStats.flying;
            Drones({fun: 'takeOff'});

          }else{
            console.log('landing');
            droneStats.flying = !droneStats.flying;
            Drones({fun: 'land'});

          }
          break;

        case 'i':
          Drones({fun: 'up', arg: 50});
          break;

        case 'j':
          Drones({fun: 'down', arg: 50});
          break;

        case 'm':
          drone2.emergencyLand();
          break;

        case 'f':
          Drones({fun: 'frontFlip'});
          break;

        case 'b':
          Drones({fun: 'backFlip'});
          break;

        default:
          break;
      }
      if (key && key.ctrl && key.name == 'c') {
        process.stdin.pause();
      }
    });
  }else{
    // let's check back in a second to see if we're ready yet.
    console.log('We\'re connecting all drones...');
    if(!connectionsRequested){
      connectionsRequested = true;
      dronelist.map(function(drone){
        drone.connect(function(){
          console.log(drone.uuid + ' is connected.');

          // We should probably detect if the battery gets too low to do flips
          // and stuff and then shut it off so it doesn't murder nearby drones 
          // with its inability to be awesome.
          drone.on('battery', function(level){
            drone.batt = level;
          });
          readylist.push('imready');
        });
      });

      setTimeout(function(){
        // wait 15 seconds for all the drones to connect, if they take longer, forget them.
        connectionTimeoutReached = true;
      }, 15000);
    }
    setTimeout(arguments.callee, 1000);
  }
};

process.stdin.setRawMode(true);
process.stdin.resume();
